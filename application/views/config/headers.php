<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
    <title>Mobil Kamu</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
    <!-- navbar style -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/article/css/navbar-style.css">
    <!-- umum -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/article/css/style.css">
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" href="<?= base_url() ?>/assets/home/img/logosjismall.png">
</head>
<body>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<div class="container">
	<div class="row">

		<div class="navbar navbar-default navbar-fixed-top" >
			<header id="header-site" style="border-bottom: 2px solid #E0E0E0 !important;">

				<div class="logo-site">
					<a href="<?php echo base_url()?>"><img src="<?= base_url()?>assets/home/img/logo.png" alt=""></a>
          
				</div>

				<!--Begin::Mobile View Navigation-->
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<!--End::Mobile View Navigation-->

				<!--Begin::Header right-->

				<!--End::Header Right-->


				<!--Begin::Nav-->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


          <ul class="nav navbar-right top-nav">
         
  				</ul>

				</div>
				<!--End::Nav-->

			</header>
		</div>
	</div>
</div>


 <?php $this->load->view($content); ?>
