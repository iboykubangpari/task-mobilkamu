<!-- jangan pernah di oprek -->
<style>
		body {
			 background-color: #ecf0f1;
		}
		.tulisanthumb{
			color: #424242;
			font-size: 15px;
		}
		.tulisanactive{
			color: #E63958;
			font-size: 15px;
		}

		hr {
				margin-top: 10px;
		    display: block;
		    height: 1px;
		    border: 0;
		    border-top: 1px #ccc;
		    padding: 0;
				margin-bottom: 0px;
		}
		.btn-file {
				border-radius: 0px;
				background-color: #e63958;
				overflow: hidden;
				color: #fff;
		}
		.btn-file input[type=file] {
				position: absolute;
				top: 0;
				right: 0;
				min-width: 100%;
				min-height: 100%;
				font-size: 100px;
				text-align: right;
				filter: alpha(opacity=0);
				opacity: 0;
				outline: none;
				background: white;
				cursor: inherit;
				display: block;
		}
</style>
<br><br><br><br>

<div class="">
	<div class="">
		<div class="col-lg-3">
			<div class="bg-white" style="padding:25px;margin:0px;">
				<?php $this->load->view('config/sidebar') ?> <!-- Load sidebar -->
			</div>
		</div>
		<!-- sampai sini jangan di oprek -->
		<div class="col-lg-9" >
			<?php $this->load->view($views) ?>
		</div>
	</div>
</div>

<br><br>
