<!-- jangan pernah di oprek -->
<style>
		body {
			 background-color: #ecf0f1;
		}
		.tulisanthumb{
			color: #424242;
			font-size: 15px;
		}
		.tulisanactive{
			color: #E63958;
			font-size: 15px;
		}

		hr {
				margin-top: 10px;
		    display: block;
		    height: 1px;
		    border: 0;
		    border-top: 1px #ccc;
		    padding: 0;
				margin-bottom: 0px;
		}
		.btn-file {
				border-radius: 0px;
				background-color: #e63958;
				overflow: hidden;
				color: #fff;
		}
		.btn-file input[type=file] {
				position: absolute;
				top: 0;
				right: 0;
				min-width: 100%;
				min-height: 100%;
				font-size: 100px;
				text-align: right;
				filter: alpha(opacity=0);
				opacity: 0;
				outline: none;
				background: white;
				cursor: inherit;
				display: block;
		}
</style>


<link rel="stylesheet" href="<?php echo base_url()?>assets/article/css/input-style.css">

<div class="row">
	<div class="container bg-white">
		<!-- sampai sini jangan di oprek -->
		<br>
		<div class="col-lg-12">
			
			<!-- for -->
		    <?php foreach ($mobil as $key) { ?>
		    <div >
		    <a href="<?= base_url().'Config/page3?id='.$key->carid?>" id="modal" style="text-decoration:none;">
				<div class="col-md-4" style="border: 1px solid #fff;padding : 6px;border-radius: 6px;">
					<div class="cover-photo">
						<?php 
							$imagename = '';
							$cek = $this->db->where('imageid',$key->carid)->get('image')->row();
							if ($cek){
								$imagename = $cek->imagename;
							}
						?>
						<img class="center-cropped-menu" src="<?= base_url().'assets/image/'.$imagename ?>"/>
						
					</div>
			
				<div style="margin-top:130px;">
					
					<div class="bg-white" style="padding-top:5px;">
						<span class="blacks"><?= $key->brandn.' '.$key->name ?></span>
					</div>
					<div class="" style="padding:5px;color: white">
						<strong><span class="" style="color: black;">Mulai Rp. <?= number_format($key->price) ?></span></strong>
					</div>
				</div>
			<br>
			</div>
			
		</a>
			</div>
		<?php } ?>
		</div>
			<!-- buat paging -->
		</div>
</div>

<br><br>
	<!-- end of modal -->

<script src="<?= base_url()?>assets/jscustom/url.min.js"></script>
<script src="<?= base_url()?>assets/jscustom/galeri.min.js"></script>