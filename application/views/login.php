<title>Login</title>

<!-- Message Notification here  -->

<link rel="stylesheet" href="<?php echo base_url()?>assets/article/css/input-style.css">


<br><br><br><br>


      <div class="container">
      <div class="row">
        <div class="col-md-5 col-md-offset-3 text-center">
          <center>
            <br><br>
          <!-- <img class="img-responsive" src="<?php echo base_url ('assets/article/img/bismillah.png');?>" width="92%"> -->
            <br><br>
          <span class="abu"><h3>Masuk</h3></span></center>
            <div class="panel-body">
              <form method="post" action="<?php echo base_url();?>main/login" method="post" id="login">

                <div class="group">
                  <input type="username" required name="username">
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Username</label>
                </div>
                <div class="group">
                  <input type="password" required name="password">
                  <span class="highlight"></span>
                  <span class="bar"></span>
                  <label>Password</label>
                </div>

                <center>
                <span class="button-download">
                  <button class="btns"  type="submit" style="width: 100%; margin: 0px;">Masuk</button>
                </span>
               </form>

            </div>
        </div>
      </div>
    </div>

<?php $msg = $this->session->flashdata('msg1'); if((isset($msg)) && (!empty($msg))) { ?>
<div class="alert alert-danger">
 <button class="close" data-dismiss="alert">x</button>
 <?php print_r($msg); ?>
</div>
<?php } ?>



<?php $msg = $this->session->flashdata('msg'); if((isset($msg)) && (!empty($msg))) { ?>
<div class="alert alert-success">
 <button class="close" data-dismiss="alert">x</button>
 <?php print_r($msg); ?>
</div>
<?php } ?>

  <script type="text/javascript">
    (function () {
      $(function () {
        $('.login--container').removeClass('preload');
        this.timer = window.setTimeout(function (_this) {
          return function () {
            return $('.login--container').toggleClass('login--active');
          };
        }(this), 2000);

        return $('.js-toggle-login').click(function (_this) {
          return function () {
            window.clearTimeout(_this.timer);
            $('.login--container').toggleClass('login--active');
            return $('.login--username-container input').focus();
          };
        }(this));
      });
    }.call(this));
  </script>
