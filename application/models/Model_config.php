<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_config extends CI_Model {

		function get_option() {
		    $this->db->select('*');
		    $this->db->from('post_category');
		    $query = $this->db->get();
		    return $query->result();
		}


    function get_menuadmin($content){
      $db = $this->db->where('class','0')->order_by('order','asc')->get('menu')->result();
      $menu = array();
      foreach ($db as $main){
        $new =[
          'name' => $main->name,
          'link' => $main->link,
          'description' => $main->description,
          'family' => $main->family 
        ];
        if ($content == $main->family){
          array_push($new,"tulisanactive"); 
        }else{
          array_push($new,"tulisanthumb");  
        }
        array_push($menu, $new);
      }
      return $menu;
    }

    function get_menucontent($isi,$family){
      $db = $this->db->where('family',$family)->where('active','1')->where('class != 0')->get('menu')->result();
      $menu = array();
      foreach ($db as $main){
        $new =[
          'name' => $main->name,
          'link' => $main->link,
          'description' => $main->description,
          'family' => $main->family 
        ];
        if ($isi == strtolower($main->uniqname)){
          array_push($new,"navbar-log"); 
        }else{
          array_push($new,"navbar-rm");  
        }
        array_push($menu, $new);
      }
      return $menu;
    }

    function getdetail($table,$param,$isi){
      $this->db->where($param,$isi);
      return $this->db->get($table)->row();
    }

    function getmenuhome($content){
      $query = $this->db->where('is_main_menu','0')->get('menu')->result();
      $menu = array();
      foreach ($query as $main) {
          $new = [
                  'id' => $main->id,
                  'menu_title' => $main->menu_title,
                  'family' => $main->family,
                  'link' => $main->link,
                  'is_main_menu' => $main->is_main_menu,
                  'anak' =>$this->db->where('is_main_menu', $main->id)->get('menu')->result()
                  ];
          // $anak = $this->db->where('is_main_menu', $main->id)->get('menu')->result();
          // array_push($new,$anak);
          if ($content == strtolower($main->menu_title)) {
            array_push($new,"active"); 
            }else{
              array_push($new,"");  
            }
            array_push($menu, $new);
          }
      return $menu;
    }

    function getbanner($family){
      $this->db->select('A.*, B.*');
      $this->db->where('A.family',$family);
      $this->db->join('image B', 'A.id = B.parentid', 'left');
      $db = $this->db->get('isi A')->result();
      return $db;
    }

    function getproducttumb($family){
      $db = $this->db->where('family',$family)->get('isi')->result();
      $menu = array();
      foreach ($db as $main){
        $new =[
          'id' => $main->id,
          'uniqname' => $main->uniqname,
          'title' => $main->title,
          'content' => $main->content,
          'category' => $main->category,
          'imagename' => $this->db->where('parentid',$main->id)->get('image')->row()->imagename
        ];
        array_push($menu, $new);
      }
      return $menu;
    }

    function getisi($uniqname){
      $db = $this->db->where('uniqname', $uniqname)->get('isi')->row();
      return $db;
    }

    function getproduct($uniqname){
      $db = $this->db->where('id', $uniqname)->get('isi')->row();
      return $db;
    }


    public function get_current_record($limit, $start) 
    {
        $this->db->limit($limit, $start);
        $query = $this->db->where('status','1')->order_by('created','desc')->get("posts");
 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
        return false;
    }
     
    public function get_total() 
    {
        return $this->db->where('status','1')->count_all("posts");
    }

}?>
