<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
        
        public $status; 
        public $roles;
    
        function __construct(){
            parent::__construct();

            $this->load->library('form_validation');    
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if( ! $this->session->userdata('email')){
                
            redirect('main/login');
            $this->session->set_flashdata('msg', 'Kamu belum login. silakan login disini');
            }
        }      


	public function index()
	{  
        $data['title'] = 'Beranda' ;
        $data['home'] = 'active';
        $data['master'] = '';
        $data['datapasien'] = '';
        $data['datadokter'] = '';
        $data['datapegawai'] = '';
        $data['datatindakan'] = '';
        $data['datadiagnosa'] = '';
        $data['datapengguna'] = '';
        $data['dataobat'] = '';
        $data['jadwaldokter'] = '';
        $data['lokasipraktek'] = '';
        $data['rekammedik'] = '';
        $data['pendaftaran'] = '';
        $data['riwayatrekammedik'] = '';
        $data['apotek'] = '';
        $data['keuangan'] = '';
        $data['laporan'] = '';
        $data['laporanpemeriksaan'] = '';
        $data['laporanrekammedik'] = '';
        $data['laporantransaksi'] = '';
        $data['laporandokterpemeriksa'] = '';
        $data['pengaturan'] = '';
        $data['pengguna'] = '';

        $data['content']='home/home';
        $this->load->view('home/header',$data);

	}
        
}
