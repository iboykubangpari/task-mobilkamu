<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

public function __construct()
	{
        parent::__construct();
        $this->load->database();
        $this->load->model('model_article');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');

}
	public function index()
	{

        $key= $this->input->get('search'); //method get key
        $page=$this->input->get('per_page');  //method get per_page

        $search=array(
            'title'=> $key,
        ); //array pencarian yang akan dibawa ke model

        $batas=9; //jlh data yang ditampilkan per halaman
        if(!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url().'article/index?key='.$key;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->model_article->jumlah_menu($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
        $config['attributes'] = array('class'=>'btn navbar-rms');
        $config['full_tag_open'] = '<div class="navbar-rms">';
        $config['full_tag_close'] = '</div>';


        $config['next_link'] = 'Next ';

        $config['prev_link'] = ' Prev';

        $config['cur_tag_open'] = '<span class="navbar-logs"><button type="button" class="btn"><a href="">';
        $config['cur_tag_close'] = '</a></button></span>';

        $this->pagination->initialize($config);
        $data['paging']=$this->pagination->create_links();
        $data['jlhpage']=$page;
        $data['title'] = '<p>showing search result for <strong>'.$key.'</strong></p>';
        $data['menu'] = $this->model_article->get_allmenu($batas,$offset,$search); //query model semua barang

        //$data['post'] = $this->model_article->tampil_data()->result();
        $data['most'] = $this->model_article->tampil_data_most()->result();
				$data['content']= ('article/home');
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('article/headers',$data);
        //$this->load->view('dashboard/footer');
        //$this->load->view('dashboard/footer');

	}

    public function s(){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/s')); exit;
        }
        $key= $this->input->get('search'); //method get key
        $page=$this->input->get('per_page');  //method get per_page

        $search=array(
            'title'=> $key,
        ); //array pencarian yang akan dibawa ke model

        $batas=12; //jlh data yang ditampilkan per halaman
        if(!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url().'article/s?key='.$key;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->model_article->jumlah_menu($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
        $config['attributes'] = array('class'=>'btn navbar-rms');
        $config['full_tag_open'] = '<div class="navbar-rms">';
        $config['full_tag_close'] = '</div>';


        $config['next_link'] = 'Next ';

        $config['prev_link'] = ' Prev';

        $config['cur_tag_open'] = '<span class="navbar-logs"><button type="button" class="btn"><a href="">';
        $config['cur_tag_close'] = '</a></button></span>';

        $this->pagination->initialize($config);
        $data['paging']=$this->pagination->create_links();
        $data['jlhpage']=$page;

        $data['title'] = 'showing search result for '.$key;
        $data['menu'] = $this->model_article->get_allmenu($batas,$offset,$search); //query model semua barang
        $data['cari']=$this->input->post('search');
        //$data['post'] = $this->model_article->tampil_data()->result();
        $data['most'] = $this->model_article->tampil_data_most()->result();
        $data['content']= ('article/list');
        //$this->load->view('article/header',$data);
        //$this->load->view('dashboard/footer');
        //$this->load->view('dashboard/footer');
    }

    public function write(){
      if(empty($this->session->userdata['email'])){
                $this->session->set_flashdata('msg1', 'you have to login first!');
                redirect(site_url().'login/');

            }
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/write')); exit;
        }
        if($this->uri->segment(3)==""){
            $offset=0;
            }else{
            $offset=$this->uri->segment(3);
            }
            $limit = 5;
            $this->session->set_userdata('row', $this->uri->segment(3));
            $data['kodeunik'] = $this->model_article->get_kode_article();
        $data['select_option']=$this->model_article->get_option();
        $data['content'] =('article/write');
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
        $this->load->view('home/headers',$data);
        //$this->load->view('dashboard/footer');
    }

     function add_article(){
        $ids=$this->session->userdata['userid']; //session ids

         $rules=[
                    [
                    'field' => 'postid',
                    'label' => 'postid'
                    ],
                    [
                    'field' => 'title',
                    'label' => 'title'
                    ],
                    [
                    'field' => 'body',
                    'label' => 'body'
                    ],
                    [
                    'field' => 'tag',
                    'label' => 'tag'
                    ],
                    [
                    'field' => 'category',
                    'label' => 'category'
                    ]

                ];
         $this->form_validation->set_rules($rules);
            if(isset($_FILES["image"]["name"]))
         {
            $config =   [
                            'upload_path'   => './uploads/article/',
                            'allowed_types' => 'gif|jpg|jpeg|png|JPEG|JPG|PNG',
                        ];


        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image'))
                {
                     echo $this->upload->display_errors();
                }
                else
                {
                 $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./uploads/article/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= TRUE;
                $config['quality']= '70%';
                $config['width']= 700;
                //$config['height']= '100%';
                $config['new_image']= './uploads/article/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();

            }
        }


         $data=[
                    'userid' => $ids,
                    'postid' => set_value('postid'),
                    'category' => set_value('category'),
                    'title' => set_value('title'),
                    'body' => set_value('body'),
                    'tag' => set_value('tag'),
                    'created' => date('Y-m-d\ H:i:s'),
                    'image' => $gbr['file_name'],

               ];

         $postid = $this->input->post('postid');
         $this->model_article->input_article($data);
         $this->session->set_flashdata('message','foto Berhasil di unggah');
         redirect('article/detail/'.$postid);

    }

    public function detail($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/detail/'.$id)); exit;
        }
        $data['get_data']= $this->model_article->get_data_edit($id);
        $data['content']=('article/detail');
        $ids=$this->session->userdata('userid'); //session id
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('home/headers',$data);
        //$this->load->view('dashboard/footer');
    }

    public function read($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/read/'.$id)); exit;
        }
        $data['get_data']= $this->model_article->get_data($id);
        $data['content']=('article/detail');
        $ids=$this->session->userdata('userid'); //session id
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('home/headers',$data);
        $this->model_article->update_counter(urldecode($id));
        //$this->load->view('dashboard/footer');
    }

    public function tag($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/tag/'.$id)); exit;
        }
       // $key= $this->input->get('search'); //method get key
        $page=$this->input->get('per_page');  //method get per_page

        $search=array(
            'tag'=> $id,

        ); //array pencarian yang akan dibawa ke model

        $batas=12; //jlh data yang ditampilkan per halaman
        if(!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url().'article/tag/'.$id;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->model_article->jumlah_menu($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
        $config['attributes'] = array('class'=>'btn navbar-rms');
        $config['full_tag_open'] = '<div class="navbar-rms">';
        $config['full_tag_close'] = '</div>';


        $config['next_link'] = 'Next ';

        $config['prev_link'] = ' Prev';

        $config['cur_tag_open'] = '<span class="navbar-logs"><button type="button" class="btn"><a href="">';
        $config['cur_tag_close'] = '</a></button></span>';

        $this->pagination->initialize($config);
        $data['paging']=$this->pagination->create_links();
        $data['jlhpage']=$page;
         $data['cari']=$this->input->post('search');
        $data['title'] = 'showing search result for '.$id;
        $data['menu'] = $this->model_article->get_allmenu($batas,$offset,$search); //query model semua barang

        //$data['post'] = $this->model_article->tampil_data()->result();
        $data['most'] = $this->model_article->tampil_data_most()->result();
        $data['content']= ('article/list');
        //$this->load->view('article/header',$data);
        //$this->load->view('dashboard/footer');
        //$this->load->view('dashboard/footer');
    }

    public function category($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/category/'.$id)); exit;
        }
       // $key= $this->input->get('search'); //method get key
        $page=$this->input->get('per_page');  //method get per_page

        $search=array(
            'category'=> $id,

        ); //array pencarian yang akan dibawa ke model

        $batas=12; //jlh data yang ditampilkan per halaman
        if(!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url().'article/tag/'.$id;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->model_article->jumlah_menu($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
        $config['attributes'] = array('class'=>'btn navbar-rms');
        $config['full_tag_open'] = '<div class="navbar-rms">';
        $config['full_tag_close'] = '</div>';


        $config['next_link'] = 'Next ';

        $config['prev_link'] = ' Prev';

        $config['cur_tag_open'] = '<span class="navbar-logs"><button type="button" class="btn"><a href="">';
        $config['cur_tag_close'] = '</a></button></span>';

        $this->pagination->initialize($config);
        $data['paging']=$this->pagination->create_links();
        $data['jlhpage']=$page;
         $data['cari']=$this->input->post('search');
        $data['title'] = 'showing search result for '.$id;
        $data['menu'] = $this->model_article->get_allmenu($batas,$offset,$search); //query model semua barang

        //$data['post'] = $this->model_article->tampil_data()->result();
        $data['most'] = $this->model_article->tampil_data_most()->result();
        $data['content']= ('article/list');
        //$this->load->view('article/header',$data);
        //$this->load->view('dashboard/footer');
        //$this->load->view('dashboard/footer');
    }

    function article(){
        if(empty($this->session->userdata['email'])){
                $this->session->set_flashdata('msg1', 'you have to login first!');
                redirect(site_url().'login/');

            }
        
        $id= $this->session->userdata['userid'];
        $data['judul'] = 'All Post';
        $data['pengumuman'] = "<div class='alert alert-success'>Jika tulisan mu sudah siap, klik tautan 'Ready to Review.' tulisan kamu akan direview terlebih dulu oleh moderator sebelum dipublikasi</div>";
        $data['article'] ='navbar-log';
        $data['draft'] ='navbar-rm';
        $data['reviewed'] ='navbar-rm';
        $data['published'] ='navbar-rm';
        $data['get_orang'] = $this->model_article->get_orang($id);
	    $data['most'] = $this->model_article->tampil_article($id)->result();
		$data['tentang'] = "navbar-btnnonactive";
		$data['faq'] = "navbar-btnnonactive";
		$data['blog'] = "navbar-btnnonactive";
		$data['mitra'] = "navbar-btnnonactive";
        $data['content']= ('article/article');
        $this->load->view('home/headers',$data);
        //$this->load->view('dashboard/footer');
    }

    function draft(){
        if(empty($this->session->userdata['email'])){
                $this->session->set_flashdata('msg1', 'you have to login first!');
                redirect(site_url().'login/');

            }
            $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/draft/')); exit;
        }
        $id= $this->session->userdata['userid'];
        $data['judul'] = 'Draft';
        $data['pengumuman'] = "<div class='alert alert-success'>Jika tulisan mu sudah siap, klik tautan 'Ready to Review.' tulisan kamu akan direview terlebih dulu oleh moderator sebelum dipublikasi</div>";
        $data['article'] ='navbar-rm';
        $data['draft'] ='navbar-log';
        $data['reviewed'] ='navbar-rm';
        $data['published'] ='navbar-rm';
        $data['get_orang'] = $this->model_article->get_orang($id);
        $data['most'] = $this->model_article->tampil_article_d($id)->result();
        $data['content']= ('article/article');
				$data['tentang'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnactive";
				$data['mitra'] = "navbar-btnnonactive";

        $this->load->view('home/headers',$data);
        //$this->load->view('dashboard/footer');
    }

    function reviewed(){
        if(empty($this->session->userdata['email'])){
                $this->session->set_flashdata('msg1', 'you have to login first!');
                redirect(site_url().'login/');

            }
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/reviewed/')); exit;
        }
        $id= $this->session->userdata['userid'];
        $data['judul'] = 'Reviewed';
        $data['pengumuman'] = "";
        $data['article'] ='navbar-rm';
        $data['draft'] ='navbar-rm';
        $data['reviewed'] ='navbar-log';
        $data['published'] ='navbar-rm';
        $data['get_orang'] = $this->model_article->get_orang($id);
        $data['most'] = $this->model_article->tampil_article_r($id)->result();
        $data['content']= ('article/article');
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('home/headers',$data);
        //$this->load->view('dashboard/footer');
    }

    function published(){
        if(empty($this->session->userdata['email'])){
                $this->session->set_flashdata('msg1', 'you have to login first!');
                redirect(site_url().'login/');

            }
        $id= $this->session->userdata['userid'];
        $data['judul'] = 'Published';
        $data['pengumuman'] = "";
        $data['article'] ='navbar-rm';
        $data['draft'] ='navbar-rm';
        $data['reviewed'] ='navbar-rm';
        $data['published'] ='navbar-log';
        $data['get_orang'] = $this->model_article->get_orang($id);
        $data['most'] = $this->model_article->tampil_article_p($id)->result();
        $data['content']= ('article/article');
				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('home/headers',$data);
        //$this->load->view('article/header',$data);
        //$this->load->view('dashboard/footer');
    }

     function delete($id)
    {

        $this->model_article->updateactivated($id);
        $this->session->set_flashdata('msg1','Artikel Berhasil Dihapus');
        redirect('article/article');
    }

     function mereview($id){
        $this->model_article->updatereviewed($id);
        $this->session->set_flashdata('msg1','Artikel akan di Review oleh moderator');
        redirect('article/article');
     }

    function edit($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/edit/'.$id)); exit;
        }
        $ids=$this->session->userdata('userid'); //session id
        $data['select_option']=$this->model_article->get_option();
        $data['get_data']= $this->model_article->get_data_edit($id);
        $data['content']=('article/edit');

				$data['tentang'] = "navbar-btnnonactive";
				$data['faq'] = "navbar-btnnonactive";
				$data['blog'] = "navbar-btnnonactive";
				$data['mitra'] = "navbar-btnnonactive";
				$this->load->view('home/headers',$data);
        $this->model_article->update_counter(urldecode($id));
        //$this->load->view('dashboard/footer');
    }

     function edit_article_action($id){
        $ids=$this->session->userdata['userid']; //session ids

         $rules=[
                    [
                    'field' => 'postid',
                    'label' => 'postid'
                    ],
                    [
                    'field' => 'title',
                    'label' => 'title'
                    ],
                    [
                    'field' => 'body',
                    'label' => 'body'
                    ],
                    [
                    'field' => 'tag',
                    'label' => 'tag'
                    ],
                    [
                    'field' => 'category',
                    'label' => 'category'
                    ]

                ];
         $this->form_validation->set_rules($rules);
         $data=[
                    'postid' => set_value('postid'),
                    'category' => set_value('category'),
                    'title' => set_value('title'),
                    'body' => set_value('body'),
                    'tag' => set_value('tag'),
                    'modified' => date('Y-m-d\ H:i:s'),


               ];
        $this->model_article->updatemenu($id,$data);

        $fotoid = $this->model_article->namafoto($id);//mengambil nama foto
        $foto = $fotoid->image;

        if(isset($_FILES["image"]["name"]))
         {
            $config =   [
                            'upload_path'   => './uploads/article/',
                            'allowed_types' => 'gif|jpg|jpeg|png|JPEG|JPG|PNG',
                        ];


        $this->upload->initialize($config);
        if(!$this->upload->do_upload('image'))
                {
                     echo $this->upload->display_errors();
                }
                else
                {
                 $gbr = $this->upload->data();
                //Compress Image
                $config['image_library']='gd2';
                $config['source_image']='./uploads/article/'.$gbr['file_name'];
                $config['create_thumb']= FALSE;
                $config['maintain_ratio']= TRUE;
                $config['quality']= '70%';
                $config['width']= 700;
                //$config['height']= '100%';
                $config['new_image']= './uploads/article/'.$gbr['file_name'];
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();
                $this->image_lib->clear();
                unlink('./uploads/article/'.$foto);
                $data=['image' => $gbr['file_name']];
                $this->model_article->updatemenu($id,$data);
            }
        }
        $postid = $this->input->post('postid');
        $this->model_article->updatestatus($postid);
         $this->session->set_flashdata('msg1','Artikel mu berhasil diperbarui ! dan akan dipublish jika lolos quality control');
         redirect('article/detail/'.$postid);

    }

    function user($id){
        $detect = new Mobile_Detect();
        if ($detect->isMobile() || $detect->isTablet() || $detect->isAndroidOS()) {
        redirect(base_url('m/article/user/'.$id)); exit;
        }
        $page=$this->input->get('per_page');  //method get per_page

        $search=array(
            'posts.userid'=> $id,

        ); //array pencarian yang akan dibawa ke model

        $batas=12; //jlh data yang ditampilkan per halaman
        if(!$page):     //jika page bernilai kosong maka batas akhirna akan di set 0
           $offset = 0;
        else:
           $offset = $page; // jika tidak kosong maka nilai batas akhir nya akan diset nilai page terakhir
        endif;

        $config['page_query_string'] = TRUE; //mengaktifkan pengambilan method get pada url default
        $config['base_url'] = base_url().'article/user/'.$id;   //url yang muncul ketika tombol pada paging diklik
        $config['total_rows'] = $this->model_article->jumlah_orang($search); // jlh total barang
        $config['per_page'] = $batas; //batas sesuai dengan variabel batas

        $config['uri_segment'] = $page; //merupakan posisi pagination dalam url pada kesempatan ini saya menggunakan method get untuk menentukan posisi pada url yaitu per_page
        $config['attributes'] = array('class'=>'btn navbar-rms');
        $config['full_tag_open'] = '<div class="navbar-rms">';
        $config['full_tag_close'] = '</div>';


        $config['next_link'] = 'Next ';

        $config['prev_link'] = ' Prev';

        $config['cur_tag_open'] = '<span class="navbar-logs"><button type="button" class="btn"><a href="">';
        $config['cur_tag_close'] = '</a></button></span>';

        $this->pagination->initialize($config);
        $data['paging']=$this->pagination->create_links();
        $data['jlhpage']=$page;
        $data['total_article']=$this->model_article->jumlah_orang($search);
         $data['cari']=$this->input->post('search');
        $data['title'] = 'showing search result for '.$id;
        $data['menu'] = $this->model_article->get_allmenu_orang($batas,$offset,$search); //query model semua barang
        $data['get_orang'] = $this->model_article->get_orang($id);
        //$data['post'] = $this->model_article->tampil_data()->result();
        $data['most'] = $this->model_article->tampil_data_most()->result();
        $data['content']= ('article/orang');
        //$this->load->view('article/header',$data);
        //$this->load->view('dashboard/footer');
        //$this->load->view('dashboard/footer');
    }

}?>
