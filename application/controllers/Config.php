<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {

public function __construct()
	{
        parent::__construct();
        $this->load->database();
        $this->load->model('model_config');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->library('upload');
}
	public function index()
	{		
		 redirect('config/content/'.'about', 'refresh');
	}

	public function content()
	{		
		$content = $this->uri->segment('2');
		$mn = $this->model_config->get_menuadmin($content);
		$menucontent = $this->model_config->get_menucontent($content,$content);
		$menudetail = $this->model_config->getdetail("menu","uniqname",$content);
		$data['menuadmin'] = $mn;
		$data['menucontent'] = $menucontent;
		$data['judul'] = $menudetail->name;
        $data['script'] = $menudetail->script;
		$data['content']= ('config/home');
		$data['views'] = "config/".$menudetail->view;
		$this->load->view('config/headers',$data);
	}

	public function page2()
	{		
		$content = $this->uri->segment('2');
		$mn = $this->model_config->get_menuadmin($content);
		$menucontent = $this->model_config->get_menucontent($content,$content);
		$menudetail = $this->model_config->getdetail("menu","uniqname",$content);
		$data['menuadmin'] = $mn;
		$this->db->join('brand', 'brand.id = car.brand');
		$data['mobil'] = $this->db->get('car')->result(); 
		$data['menucontent'] = $menucontent;
		$data['judul'] = $menudetail->name;
        $data['script'] = $menudetail->script;
		$data['content']= ('config/home');
		$data['views'] = "config/".$menudetail->view;
		$this->load->view('config/headers',$data);
	}

	public function page3()
	{	
		$id = $this->input->get('id');
		$content = $this->uri->segment('2');
		echo($content);
		$mn = $this->model_config->get_menuadmin($content);
		$menucontent = $this->model_config->get_menucontent($content,$content);
		$menudetail = $this->model_config->getdetail("menu","uniqname",$content);
		$data['foto'] = $this->db->where('imageid',$id)->get('image')->result();
		echo json_encode($this->db->where('imageid',$id)->get('image')->result());
		$this->db->join('brand', 'brand.id = car.brand');
		$this->db->join('fuel', 'fuel.id = car.fuel');
		$this->db->join('cartype', 'cartype.id = car.category');

		$data['mobil'] = $this->db->where('carid',$id)->get('car')->row();
		$data['menuadmin'] = $mn;
		$data['menucontent'] = $menucontent;
		$data['judul'] = $menudetail->name;
        $data['script'] = $menudetail->script;
		$data['content']= ('config/home');
		$data['views'] = "config/".$menudetail->view;
		$this->load->view('config/headers',$data);
	}
   
}?>
