<?php
defined('BASEPATH') OR exit('No direct script acces allowed');

class Api extends CI_Controller {

public function __construct()
	{

		parent::__construct();
		$this->load->database();
		$this->load->helper('form');
	}

	
	function brand(){
		echo json_encode(['result'=>$this->db->get('brand')->result()]);
	}

	function cartype(){
		echo json_encode(['result'=>$this->db->get('cartype')->result()]);
	}

	function fuel(){
		echo json_encode(['result'=>$this->db->get('fuel')->result()]);
	}

	function selectcar(){
		$this->db->join('brand B', 'B.id = A.brand');
		$this->db->join('cartype C', 'C.id = A.category');
		$this->db->join('fuel D', 'D.id = A.fuel');
		$result = $this->db->get('car A')->result();
		echo json_encode(['result'=>$result]);
	}

	function insertcar(){
		$f1 = $this->input->post('image1');
		$f2 = $this->input->post('image2');
		$f3 = $this->input->post('image3');
		$f4 = $this->input->post('image4');

		$id = $this->input->post('id');

		if ($f1 != '') {
			$this->uploader($f1,$id);
		}if ($f2 != '') {
			$this->uploader($f2,$id);
		}if ($f3 != '') {
			$this->uploader($f3,$id);
		}if ($f4 != '') {
			$this->uploader($f4,$id);
		}

		$data = [	'carid' => $id,
					'name' => $this->input->post('name'),
					'category'  => $this->input->post('category'),
					'brand'  => $this->input->post('brand'),
					'model'  => $this->input->post('model'),
					'fuel'  => $this->input->post('fuel'),
					'price'  => $this->input->post('price')
				];
		$result = $this->db->insert('car',$data);
		if ($result) {
			echo(json_encode("ss"));
			// redirect(base_url().'Config/page3?id'.$id);
		}
	}

	function uploader($datas,$id){
            //decoding base64 string value
		list($ketarangan, $foto) = explode(",", $datas);

		$image = base64_decode($foto);

        $image_name = md5(uniqid(rand(), true));
        //encryp name
        $filename = $image_name.'.'.'png';
        //image upload path
        file_put_contents('./assets/image/' . $filename, $image);

        $config['image_library']='gd2';
        $config['source_image']='./assets/image/'.$filename;
        $config['create_thumb']= FALSE;
        $config['maintain_ratio']= TRUE;
        // $config['quality']= '80%';
        $config['width']= 1200;
            //$config['height']= '100%';
        $config['new_image']= './assets/image/'.$filename;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        $data = ['imagename' => $filename,'imageid' => $id];
        $this->db->insert('image',$data);
        return $filename;
	}

	function showimage(){
		$id = $this->input->get('id');
		echo json_encode(['result' => $this->db->where('imageid',$id)->get('image')->result()]);
	}

}?>


